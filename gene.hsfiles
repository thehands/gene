{-# START_FILE Main.hs #-}
module Main where

import Control.Lens.Lens (lens, Lens')
import Snap.Http.Server.Config (defaultConfig)
import Snap.Snaplet
import Snap.Snaplet.Heist
import Snap.Util.FileServe (serveDirectoryWith, simpleDirectoryConfig)

data App = App { _heist :: Snaplet (Heist App) }

heist :: Lens' App (Snaplet (Heist App))
heist = lens _heist (\app v -> app { _heist = v })


{{name}}Init :: SnapletInit App App
{{name}}Init = makeSnaplet "{{name}}" "Description." Nothing $ do
    addRoutes [ ("static", serveDirectoryWith simpleDirectoryConfig "static")
              , ("", heistServe)
              ]
    h <- nestSnaplet "heist" heist $ heistInit ""
    addTemplatesAt h "" "./templates"
    return $ App h


instance HasHeist App where heistLens = subSnaplet heist


main :: IO ()
main = serveSnaplet defaultConfig {{name}}Init

{-# START_FILE {{name}}.cabal #-}
Name:          {{name}}
Version:       0.1.0.0
Synopsis:      Project Synopsis Here.
Description:   Project Description Here.
Homepage:      {{name}}.com
License:       AllRightsReserved
Author:        Author
Maintainer:    maintainer@{{name}}.com
Category:      Web
Build-type:    Custom
Cabal-version: >= 1.10

Executable {{name}}
  Main-is: Main.hs
  Default-language: Haskell2010
  Default-extensions:
    OverloadedStrings
  GHC-options:
    -Wall
    -fwarn-tabs
    -fno-warn-unused-do-bind
    -funbox-strict-fields
    -O2
    -threaded
  Build-depends:
    base          >= 4   && < 5,
    lens          >= 4   && < 5,
    monad-control >= 1.0 && < 1.1,
    mtl           >= 2   && < 3,
    snap          >= 1.0 && < 1.1,
    snap-core     >= 1.0 && < 1.1,
    snap-server   >= 1.0 && < 1.1,
    transformers  >= 0.4 && < 0.6

{-# START_FILE stack.yaml #-}
resolver: lts-7.3

packages:
- '.'

extra-deps:
- snap-1.0.0.1
- heist-1.0.0.0
- map-syntax-0.2.0.1

{-# START_FILE .gitignore #-}
*.swp
.DS_Store
.cabal-sandbox
.stack-work/
cabal.sandbox.config
log
site_key.txt
style.css

{-# START_FILE templates/write-templates-here #-}
Files are automaticaly served, prepend with `_` to prevent this.
