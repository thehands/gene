module Main where

import qualified Data.List as L
import qualified Data.ByteString.Char8 as B
import Blaze.ByteString.Builder
import Control.Applicative ((<$>))
import Control.Concurrent (threadDelay)
import Control.Lens ((&), (.~))
import Control.Monad (forever)
import Data.Char (toLower)
import Data.Map.Syntax ((##))
import Heist
import Heist.Splices.Apply
import Heist.Splices.Bind
import Heist.Splices.Html
import Heist.Splices.Ignore
import Heist.Interpreted as HI
import System.Directory (listDirectory, doesDirectoryExist)
import System.Exit (exitSuccess)
import System.FSNotify as N
import Options

{- Get template files -}


-- Get all filepaths from a path.
getFilePaths :: FilePath -> IO [FilePath]
getFilePaths path = do
    dirExists <- doesDirectoryExist path
    if dirExists
      then do
        dir <- listDirectory path
        let fullpaths = ((path ++ "/") ++) <$> filter ((/= '.') . head) dir
        concat <$> mapM getFilePaths fullpaths
      else return [path]

-- Predicate for tpl files.
isTplFile :: FilePath -> Bool
isTplFile fp = L.isSuffixOf ".tpl" $ fmap toLower fp

getTplName :: String -> String
getTplName = takeWhile ('.'/=) . reverse . takeWhile (/='/') . reverse

-- Get all .tpl file names.
getTplNames :: FilePath -> IO [String]
getTplNames path = do
  flps <- getFilePaths path
  return $ filter (not . L.isPrefixOf "_") $
    map getTplName $ filter isTplFile flps


{- Render Templates -}

-- Render a template.
renderTpl :: HeistState IO -> String -> IO ()
renderTpl hs name = do
  mb <- HI.renderTemplate hs $ B.pack name
  case fmap (toByteString . fst) mb of
    Nothing -> putStrLn $ "Template, `" ++ name ++ "` not found."
    Just "" -> putStrLn $ "Template file, `" ++ name ++ ".tpl` was empty. Nothing was rendered."
    Just b  -> do
      B.putStrLn $ B.append "Rendered template: " $ B.pack $ name ++ ".tpl"
      B.writeFile (name ++ ".html") $ B.dropWhile (\a -> a==' ' || a=='\n') b

-- Render a list of template names.
renderAll :: HeistState IO -> [String] -> IO ()
renderAll _ [] = B.putStrLn "No templates rendered."
renderAll hs names = mapM_ (renderTpl hs) names

{- Main -}


main :: IO ()
main = do
  opts <- getOpts
  whenFlag Help opts $ do
    putStrLn
      "Gene, Help\n\
      \Usage: \n\
      \  `gene` to render all templates in PWD and\n\
      \    subfolders not starting with _ e.g. index.tpl\n\
      \    not _index.tpl. or,\n\
      \  `gene -t index` or,\n\
      \  `gene -t index.tpl`\n\
      \Options:\n\
      \  -v -V,  --version  Print version, exit.\n\
      \  -h,     --help     Print this help, exit.\n"
    exitSuccess
  whenFlag Version opts $ do
    putStrLn "Gene, Static site generator, Version 0.2.0"
    exitSuccess
  whenFlag Watch opts $ do
    inotify <- initINotify
    _ <- I.addWatch inotify [I.CloseWrite] (getArg 0 "." opts) $ \e ->
      case e of
        (I.Closed _ (Just f) _) -> if ".tpl" `L.isInfixOf` f
          then do
            putStrLn "Change detected, regenerating site."
            ehs <- initHeist $ emptyHeistConfig
              & hcLoadTimeSplices .~ do
                applyTag    ## applyImpl
                bindTag     ## bindImpl
                ignoreTag   ## ignoreImpl
                htmlTag     ## htmlImpl
              & hcTemplateLocations .~ [loadTemplates "."]
              & hcNamespace .~ ""
            ns <- getTplNames $ getArg 0 "." opts
            renderAll (either (error . show) id ehs) ns
          else pure ()
        _ -> pure ()
    putStrLn "Watching templates for changes.."
    forever $ threadDelay 1000000
  ehs <- initHeist $ emptyHeistConfig
    & hcLoadTimeSplices .~ do
      applyTag    ## applyImpl
      bindTag     ## bindImpl
      ignoreTag   ## ignoreImpl
      htmlTag     ## htmlImpl
    & hcTemplateLocations .~ [loadTemplates "."]
    & hcNamespace .~ ""
  ns <- getTplNames $ getArg 0 "." opts
  renderAll (either (error . show) id ehs) ns