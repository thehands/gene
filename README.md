# Gene
web project templating and static site generation.

Concept: You have a real idea, prototype in a real environment.

# What
A seed, web-project template designed to make reduce the friction from "what-if?" to "yes!".

# Why
Steven Wilson (a legendary musician) always records in high definition. 

# How
Heist templating with cell (auto-rebuilding) and some basic js tools included, e.g. electrical.js

**Example Usage**
- Initialize: `$ gene my-project` or `gene my-project -t my-template`
- Develop: `$ gene -d` in develop your project will be be located at localhost:8000
