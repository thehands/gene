module Options where

import System.Console.GetOpt
import System.Environment (getArgs)
import qualified Data.List as L (find)

data Symbol = Version | Help | Watch | Template
  deriving (Eq, Show)

data Opt = Flag Symbol | Key Symbol String
  deriving (Eq, Show)

options :: [OptDescr Opt]
options =
  [ Option ['v','V'] ["version"] (NoArg (Flag Version) ) "Show version number."
  , Option ['h']     ["help"]    (NoArg (Flag Help)    ) "Show help message."
  , Option ['w']     ["watch"]   (NoArg (Flag Watch)    ) "Watch directory for changes and process them."
  ]

getOpts :: IO ([Opt], [String])
getOpts = do
  args <- getArgs
  case getOpt Permute options args of
    (o,n,[])   -> return (o,n)
    (_,_,errs) -> ioError (userError (concat errs ++ usageInfo "Options:" options))

whenFlag :: (Applicative f) => Symbol -> ([Opt], [String]) -> f () -> f ()
whenFlag sym (fl,_) a = case L.find (ifSymbol sym) fl of
  Just (Flag _) -> a
  _ -> pure ()

whenKey :: (Applicative f) => Symbol -> ([Opt], [String]) -> (String -> f ()) -> f ()
whenKey sym (kl,_) a = case L.find (ifSymbol sym) kl of
  Just (Key  _ v) -> a v
  _ -> pure ()

ifSymbol :: Symbol -> Opt -> Bool
ifSymbol sym (Flag s  ) = sym == s
ifSymbol sym (Key  s _) = sym == s

getArg :: Int -> String -> ([Opt], [String]) -> String
getArg num def (_,args) = if length args < num + 1
  then def
  else args !! num
